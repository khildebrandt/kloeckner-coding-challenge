var merge = require('webpack-merge')
var devEnv = require('./dev.env')

module.exports = merge(devEnv, {
  NODE_ENV: '"testing"',
  API_ENDPOINT: '"http://localhost:9000"',
  API_KEY: '"123"'
})
