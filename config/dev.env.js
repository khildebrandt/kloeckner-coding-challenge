var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_ENDPOINT: '"http://api.openweathermap.org/data/2.5/weather"',
  API_KEY: '"b478d1ba9d91a541ea1f58e562948c37"'
})
