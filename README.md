# ki_coding_challenge

> Coding challenge for kloeckner.i by Kristian Hildebrandt

## Build Setup

``` bash
# install dependencies with [Yarn](https://yarnpkg.com/en/docs/install)
yarn

# run unit tests
yarn unit

# run e2e tests
yarn e2e

# start dev server
yarn dev
```

# Framework Choice
- no backend necessary
- synchronous backend (ruby) not recommended
- little previous Vue knowledge so it made sense to use this opportunity to get some experience in testing and how to deal with async data

# Possible Improvements
- consolidate api mocks for unit and integration specs
- use exact city id's for more accurate results
- auto suggestions for city and country input
- sync city and country input when random location is selected (without exact city id's it is confusing because the form would be set to the inaccurate results from the api)
