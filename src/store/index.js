import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const state = {
  location: { city: 'Berlin', country: 'de' },
  gps: [0, 0]
}

export const mutations = {
  updateLocation (state, location) {
    state.location.country = location.country
    state.location.city = location.city
  },
  updateGps (state, gps) {
    state.gps = gps
  }
}

export default new Vuex.Store({
  state,
  mutations
})

