// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage
var http    =  require('http');
var mockserver  =  require('mockserver');
var server

module.exports = {
  before: function (browser) {
    server = http.createServer(mockserver('./test/mocks')).listen(9000);
  },
  after: function() {
    server.close()
  },
  'header didsplays correct info': function (browser) {
    const devServer = browser.globals.devServerURL

    browser
      .url(devServer)
      .waitForElementVisible('.weather-display', 5000)
      .assert.containsText('h1.title', 'Coding Challenge')
      .assert.containsText('p.subtitle', 'By Kristian Hildebrandt, powered by')
  },
  'displays the weather for the default location': function (browser) {
    browser
      .assert.containsText('.weather-display', 'Berlin')
  },
  'displays the weather for the changed location': function (browser) {
    browser
      .clearValue('.country input')
      .setValue('.country input', 'fr')
      .clearValue('.city input')
      .setValue('.city input', 'Paris')
      .click('#get-location')
      .assert.containsText('.weather-display', 'Paris')
  },
  'shows an error message if the city cannot be found': function (browser) {
    browser
      .clearValue('.country input')
      .setValue('.country input', 'doesnotexist')
      .clearValue('.city input')
      .setValue('.city input', 'doesnotexist')
      .click('#get-location')
      .assert.containsText('.weather-display', 'The city could not be found')
      .end()
  }
}
