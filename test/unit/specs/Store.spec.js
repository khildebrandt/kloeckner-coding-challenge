import store from '@/store'

describe('Store', () => {
  describe('Initialization', () => {
    it('sets a default location during initialization', () => {
      expect(store.state.location.country).to.eql('de')
      expect(store.state.location.city).to.eql('Berlin')
    })

    it('sets the gps coordinates to { 0, 0 }', () => {
      expect(store.state.gps).to.eql([0, 0])
    })
  })

  describe('Mutations', () => {
    it('updates the location', () => {
      store.commit('updateLocation', { country: 'foo', city: 'bar' })

      expect(store.state.location.country).to.eql('foo')
      expect(store.state.location.city).to.eql('bar')
    })

    it('updates the gps', () => {
      store.commit('updateGps', [25, 35])

      expect(store.state.gps).to.eql([25, 35])
    })
  })
})
