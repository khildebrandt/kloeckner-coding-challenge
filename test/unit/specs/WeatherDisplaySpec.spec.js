import Vue from 'vue'
import WeatherDisplay from '@/components/WeatherDisplay'
import Vuex from 'vuex'
import { mutations } from '@/store'
import moxios from 'moxios'

describe('WeatherDisplay.vue', () => {
  var vm
  var store
  var state

  beforeEach(function () {
    moxios.install()
  })

  afterEach(function () {
    moxios.uninstall()
  })

  beforeEach(() => {
    state = { location: { city: 'London', country: 'uk' }, gps: [0, 0] }
    store = new Vuex.Store({ state, mutations })
    const Constructor = Vue.extend(WeatherDisplay)
    vm = new Constructor({ store: store }).$mount()
  })

  it('displays the default weather', (done) => {
    var url = 'http://localhost:9000?q=London,uk&APPID=123'

    var response = {
      sys: { country: 'UK' },
      name: 'London',
      main: { temp: 1, humidity: 2 },
      wind: { speed: 3 }
    }

    moxios.stubRequest(url, {
      response: response
    })

    moxios.wait(function () {
      expect(vm.$el.querySelector('#wd-country').innerText).to.contain(response.sys.country)
      expect(vm.$el.querySelector('#wd-city').innerText).to.contain(response.name)
      expect(vm.$el.querySelector('#wd-temp').innerText).to.contain(response.main.temp)
      expect(vm.$el.querySelector('#wd-humidity').innerText).to.contain(response.main.humidity)
      expect(vm.$el.querySelector('#wd-wind-speed').innerText).to.contain(response.wind.speed)

      done()
    })
  })

  it('updates the weather by city', (done) => {
    store.commit('updateLocation', { city: 'Paris', country: 'fr' })
    var url = 'http://localhost:9000?q=Paris,fr&APPID=123'

    var response = {
      sys: { country: 'FR' },
      name: 'Paris',
      main: { temp: 11, humidity: 22 },
      wind: { speed: 33 }
    }

    moxios.stubRequest(url, {
      response: response
    })

    moxios.wait(function () {
      expect(vm.$el.querySelector('#wd-country').innerText).to.contain(response.sys.country)
      expect(vm.$el.querySelector('#wd-city').innerText).to.contain(response.name)
      expect(vm.$el.querySelector('#wd-temp').innerText).to.contain(response.main.temp)
      expect(vm.$el.querySelector('#wd-humidity').innerText).to.contain(response.main.humidity)
      expect(vm.$el.querySelector('#wd-wind-speed').innerText).to.contain(response.wind.speed)

      done()
    })
  })

  it('updates the weather by gps', (done) => {
    store.commit('updateGps', [10, 51])
    var url = 'http://localhost:9000?lon=10&lat=51&APPID=123'

    var response = {
      sys: { country: 'DE' },
      name: 'Richelsdorf',
      main: { temp: 111, humidity: 222 },
      wind: { speed: 333 }
    }

    moxios.stubRequest(url, {
      response: response
    })

    moxios.wait(function () {
      expect(vm.$el.querySelector('#wd-country').innerText).to.contain(response.sys.country)
      expect(vm.$el.querySelector('#wd-city').innerText).to.contain(response.name)
      expect(vm.$el.querySelector('#wd-temp').innerText).to.contain(response.main.temp)
      expect(vm.$el.querySelector('#wd-humidity').innerText).to.contain(response.main.humidity)
      expect(vm.$el.querySelector('#wd-wind-speed').innerText).to.contain(response.wind.speed)

      done()
    })
  })

  it('displays an error message for an invalid location', (done) => {
    store.commit('updateLocation', { city: 'doesnotexist', country: 'doesnotexist' })
    var url = 'http://localhost:9000?q=doesnotexist,doesnotexist&APPID=123'

    moxios.stubRequest(url, {
      code: 404
    })

    moxios.wait(function () {
      expect(vm.$el.innerText).to.contain('The city could not be found')

      done()
    })
  })
})
