import Vue from 'vue'
import WeatherForm from '@/components/WeatherForm'
import { mutations } from '@/store'
import Vuex from 'vuex'

describe('WeatherForm.vue', () => {
  var vm
  var store
  var state

  beforeEach(() => {
    state = { location: { city: '', country: '' }, gps: [0, 0] }
    store = new Vuex.Store({ state, mutations })
    const Constructor = Vue.extend(WeatherForm)
    vm = new Constructor({ store: store }).$mount()
  })

  it('set the input fields with the empty initial values', () => {
    expect(vm.$el.querySelector('.field.country input[type="text"]').value).to.eql('')
    expect(vm.$el.querySelector('.field.city input[type="text"]').value).to.eql('')
  })

  it('updates the view and store with city and country', (done) => {
    vm.country = 'Germany'
    vm.city = 'Berlin'
    vm.setLocation()

    vm.$nextTick(() => {
      expect(vm.$el.querySelector('.field.city input[type="text"]').value).to.eql('Berlin')
      expect(vm.$store.state.location.country).to.eql('Germany')
      expect(vm.$store.state.location.city).to.eql('Berlin')
      done()
    })
  })

  it('updates the store with gps choordinates', () => {
    sinon.stub(Math, 'random').returns(4)
    var getRamdomCoordinates = sinon.spy(vm, 'getRamdomCoordinates')

    vm.getRandomLocation()

    expect(getRamdomCoordinates.getCall(0).args).to.eql([0, 10])
    expect(getRamdomCoordinates.getCall(1).args).to.eql([40, 55])

    expect(vm.$store.state.gps).to.eql([40, 100])
  })
})
